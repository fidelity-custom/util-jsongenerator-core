package com.reltio.etl.service.domain;

import java.io.Serializable;
import java.util.Properties;
import java.util.regex.Pattern;

import static com.reltio.etl.constants.JsonGeneratorProperties.*;

public class InteractionConfigurationProperties implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7755465810233279828L;

	private String inputDataFilePath;
	private String inputFileFormat;
	private String inputFileMask;
	private String inputFileDelimiter;
	private String outputFilePath;
	private String rejectFilePath;
	private String attributeMappingFilePath;
	private String memberseMappingFilePath;
	private String entityType;
	private String sourceSystem;
	private String crosswalkValueColumn;
	private Integer threadCount;
	private String columnDelimiter;
	
	private String awsKey;
	private String awsSecretKey;
	private String awsRegion;

	private Boolean isSingleJsonOutput = false;
	private Boolean isJsonWithOutCrosswalk = false;

	private String timestampColumn;
	private boolean ignoreNullAttribute=true;

	public InteractionConfigurationProperties(Properties properties) {

		// READ the Config Properties values
		inputDataFilePath = properties.getProperty("INPUT_DATA_FILE");
		inputFileFormat = properties.getProperty("INPUT_FILE_FORMAT");
		inputFileMask = properties.getProperty("INPUT_FILE_MASK");
		inputFileDelimiter = properties.getProperty("INPUT_FILE_DELIMITER");
        outputFilePath = properties.getProperty("OUTPUT_FILE");
        rejectFilePath = properties.getProperty("REJECT_FILE");
		attributeMappingFilePath = properties
                .getProperty("ATTRIBUTE_MAPPING_FILE");
		memberseMappingFilePath = properties
				.getProperty("MEMBERS_MAPPING_FILE");

		entityType = properties.getProperty("INTERACTION_TYPE");
		sourceSystem = properties.getProperty("SOURCE_SYSTEM");
		crosswalkValueColumn = properties.getProperty("CROSSWALK_VALUE_COLUMN");
		
		awsKey = properties.getProperty("AWS_KEY");
		awsSecretKey = properties.getProperty("AWS_SECRET_KEY");
		awsRegion = properties.getProperty("AWS_REGION");

		columnDelimiter = properties
				.getProperty("NORMALIZED_FILE_COLUMN_DELIMITER");
		ignoreNullAttribute=Boolean.valueOf(properties.getProperty("GENERATE_NULL_VALUES"));

		if (columnDelimiter != null && !columnDelimiter.isEmpty()) {
			columnDelimiter = Pattern.quote(columnDelimiter);
		}

		String threadCountStr = properties.getProperty("THREAD_COUNT");
		if (threadCountStr == null || threadCountStr.isEmpty()) {
			setThreadCount(null);
		} else {
			setThreadCount(Integer.parseInt(threadCountStr));
		}

		String jsonFormat = properties.getProperty("JSON_OUTPUT_FORMAT");
		if (jsonFormat != null) {
			if (jsonFormat.equalsIgnoreCase(SINGLE_JSON_OUTPUT)) {
				isSingleJsonOutput = true;

			} else if (jsonFormat.equalsIgnoreCase(JSON_WITHOUT_CROSSWALK)) {
				isJsonWithOutCrosswalk = true;

			}
		}

		timestampColumn = properties.getProperty("TIMESTAMP_COLUMN");

	}

	
	public boolean isIgnoreNullAttribute() {
		return ignoreNullAttribute;
	}


	public void setIgnoreNullAttribute(boolean ignoreNullAttribute) {
		this.ignoreNullAttribute = ignoreNullAttribute;
	}


	/**
	 * @return the inputDataFilePath
	 */
	public String getInputDataFilePath() {
		return inputDataFilePath;
	}

	/**
	 * @param inputDataFilePath
	 *            the inputDataFilePath to set
	 */
	public void setInputDataFilePath(String inputDataFilePath) {
		this.inputDataFilePath = inputDataFilePath;
	}

	/**
	 * @return the inputFileFormat
	 */
	public String getInputFileFormat() {
		return inputFileFormat;
	}

	/**
	 * @param inputFileFormat
	 *            the inputFileFormat to set
	 */
	public void setInputFileFormat(String inputFileFormat) {
		this.inputFileFormat = inputFileFormat;
	}

	/**
	 * @return the inputFileMask
	 */
	public String getInputFileMask() {
		return inputFileMask;
	}


	/**
	 * @param inputFileMask the inputFileMask to set
	 */
	public void setInputFileMask(String inputFileMask) {
		this.inputFileMask = inputFileMask;
	}


	/**
	 * @return the inputFileDelimiter
	 */
	public String getInputFileDelimiter() {
		return inputFileDelimiter;
	}

	/**
	 * @param inputFileDelimiter
	 *            the inputFileDelimiter to set
	 */
	public void setInputFileDelimiter(String inputFileDelimiter) {
		this.inputFileDelimiter = inputFileDelimiter;
	}

	/**
	 * @return the outputFilePath
	 */
	public String getOutputFilePath() {
		return outputFilePath;
	}

	/**
	 * @param outputFilePath
	 *            the outputFilePath to set
	 */
	public void setOutputFilePath(String outputFilePath) {
		this.outputFilePath = outputFilePath;
	}

	/**
	 * @return the rejectFilePath
	 */
	public String getRejectFilePath() {
		return rejectFilePath;
	}


	/**
	 * @param rejectFilePath the rejectFilePath to set
	 */
	public void setRejectFilePath(String rejectFilePath) {
		this.rejectFilePath = rejectFilePath;
	}


	/**
	 * @return the entityType
	 */
	public String getEntityType() {
		return entityType;
	}

	/**
	 * @param entityType
	 *            the entityType to set
	 */
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	/**
	 * @return the sourceSystem
	 */
	public String getSourceSystem() {
		return sourceSystem;
	}

	/**
	 * @param sourceSystem
	 *            the sourceSystem to set
	 */
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	/**
	 * @return the crosswalkValueColumn
	 */
	public String getCrosswalkValueColumn() {
		return crosswalkValueColumn;
	}

	/**
	 * @param crosswalkValueColumn
	 *            the crosswalkValueColumn to set
	 */
	public void setCrosswalkValueColumn(String crosswalkValueColumn) {
		this.crosswalkValueColumn = crosswalkValueColumn;
	}

	/**
	 * @return the threadCount
	 */
	public Integer getThreadCount() {
		return threadCount;
	}

	/**
	 * @param threadCount
	 *            the threadCount to set
	 */
	public void setThreadCount(Integer threadCount) {
		if (threadCount == null) {
			this.threadCount = MIN_THREAD_COUNT;
		} else if (threadCount > MAX_THREAD_COUNT) {
			this.threadCount = MAX_THREAD_COUNT;

		} else {
			this.threadCount = threadCount;
		}
	}

	/**
	 * @return the columnDelimiter
	 */
	public String getColumnDelimiter() {
		return columnDelimiter;
	}

	/**
	 * @param columnDelimiter
	 *            the columnDelimiter to set
	 */
	public void setColumnDelimiter(String columnDelimiter) {
		this.columnDelimiter = columnDelimiter;
	}

	/**
	 * @return the attributeMappingFilePath
	 */
	public String getAttributeMappingFilePath() {
		return attributeMappingFilePath;
	}

	/**
	 * @param attributeMappingFilePath
	 *            the attributeMappingFilePath to set
	 */
	public void setAttributeMappingFilePath(String attributeMappingFilePath) {
		this.attributeMappingFilePath = attributeMappingFilePath;
	}

	/**
	 * @return the memberseMappingFilePath
	 */
	public String getMemberseMappingFilePath() {
		return memberseMappingFilePath;
	}

	/**
	 * @param memberseMappingFilePath
	 *            the memberseMappingFilePath to set
	 */
	public void setMemberseMappingFilePath(String memberseMappingFilePath) {
		this.memberseMappingFilePath = memberseMappingFilePath;
	}

	/**
	 * @return the isSingleJsonOutput
	 */
	public Boolean getIsSingleJsonOutput() {
		return isSingleJsonOutput;
	}

	/**
	 * @param isSingleJsonOutput
	 *            the isSingleJsonOutput to set
	 */
	public void setIsSingleJsonOutput(Boolean isSingleJsonOutput) {
		this.isSingleJsonOutput = isSingleJsonOutput;
	}

	/**
	 * @return the isJsonWithOutCrosswalk
	 */
	public Boolean getIsJsonWithOutCrosswalk() {
		return isJsonWithOutCrosswalk;
	}

	/**
	 * @param isJsonWithOutCrosswalk
	 *            the isJsonWithOutCrosswalk to set
	 */
	public void setIsJsonWithOutCrosswalk(Boolean isJsonWithOutCrosswalk) {
		this.isJsonWithOutCrosswalk = isJsonWithOutCrosswalk;
	}

	/**
	 * @return the timestampColumn
	 */
	public String getTimestampColumn() {
		return timestampColumn;
	}

	/**
	 * @param timestampColumn
	 *            the timestampColumn to set
	 */
	public void setTimestampColumn(String timestampColumn) {
		this.timestampColumn = timestampColumn;
	}
	/**
	 * @return the awsKey
	 */
	public String getAwsKey() {
		return awsKey;
	}


	/**
	 * @param awsKey the awsKey to set
	 */
	public void setAwsKey(String awsKey) {
		this.awsKey = awsKey;
	}


	/**
	 * @return the awsSecretKey
	 */
	public String getAwsSecretKey() {
		return awsSecretKey;
	}


	/**
	 * @param awsSecretKey the awsSecretKey to set
	 */
	public void setAwsSecretKey(String awsSecretKey) {
		this.awsSecretKey = awsSecretKey;
	}


	/**
	 * @return the awsRegion
	 */
	public String getAwsRegion() {
		return awsRegion;
	}


	/**
	 * @param awsRegion the awsRegion to set
	 */
	public void setAwsRegion(String awsRegion) {
		this.awsRegion = awsRegion;
	}
}
