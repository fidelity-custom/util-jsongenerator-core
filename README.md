
#JSON Generator Utility

## Description

The Generic JSON Generator is a Java tool developed to convert data stored in a flat or csv format into a JSON format ready to be consumed by the Relio data loader. The tool is exceptionally useful in circumstances where you are attempting to load structured data from from a traditional data platform such a relational database or excel. The tool is capable of loading entities, interactions and relations. 

The application is driven from a configuration file that allows you to specify important information about your source system, data structure, destination, and data mapping. From modifying the configuration file the use will be able to quickly prepare data to be loaded to a Reltio tenant. 

##Change Log

```
v3.1.7
LUD         :   14th July 2021
Version     :   3.1.7
LUB         :   Mallikarjuna Aakulati
Description :   modified reject file format according to Fidelity needs.

v3.1.6
LUD         :   05th July 2021
Version     :   3.1.6
LUB         :   Mallikarjuna Aakulati
Description :   Added functionality to write Reject files to a separate directory other than the Output Directory.

v3.1.5
LUD         :   11th June 2021
Version     :   3.1.5
LUB         :   Mallikarjuna Aakulati
Description :   Added functionality to read from and to write to AWS S3 Storage and to accept source table value from the input file column.

v3.1.4
LUD         :   8th Feb 2021
Version     :   3.1.4
LUB         :   Sanjay
Description :   Fix for CUST-3171

v3.1.3
LUD         :   28Aug July 2020
Version     :   3.1.3
LUB         :   Sanjay
Description :   Fix for CUST-3162

v3.1.2
LUD         :   28th July 2020
Version     :   3.1.2
LUB         :   Sanjay
Description :   Fix for CUST-3161 - GENERATE_NULL_VALUES property not working for entity and relation nested attribute

v3.1.1
LUD         :   1st July 2020
Version     :   3.1.1
LUB         :   Srujan Kumar
Description :   Code Fixed to populate Crosswalk 'type' in refRelation attribute

v3.1.0
LUD         :   29th June 2020
Version     :   3.1.0
LUB         :   Srujan Kumar
Description :   Code Fixed for handling null/empty Sub-nested normalized attributes based on GENERATE_NULL_VALUES (true/false) parameter

v3.0.8
LUD         :   4th Feb 2020
Version     :   3.0.8
LUB         :   Sanjay
Description :   Fixed the RefRelation issue with Normalized json
                
v3.0.7
LUD         :   17th Jan 2020
Version     :   3.0.7
LUB         :   Sanjay
Description :   Fixed the RefRelation issue with Denormalized json

v3.0.6
LUD         :   13th Jan 2020
Version     :   3.0.6
LUB         :   Vignesh
Description :   Bug fix for empty reference attribute in case of single static value.
                Test case for special character added.


v3.0.5
LUD         :   06th Jan 2020
Version     :   3.0.5
LUB         :   Vignesh
Description :   Bug fix for empty nested attribute in case of single static value.

v3.0.4
LUD         :   30th December 2019
Version     :   3.0.4
LUB         :   Sanjay
Description :   Added feature for passing Source and SourceTable for RefEntity and RefRelation
            
v3.0.3
LUD         :   16th September 2019
Version     :   3.0.3
LUB         :   Sanjay
Description :   Nested normalized featured added (https://reltio.jira.com/browse/ROCS-64)
                Test Case added
                
                
v3.0.3
LUD         :   16th September 2019
Version     :   3.0.3
LUB         :   Sanjay
Description :   Nested normalized featured added (https://reltio.jira.com/browse/ROCS-64)
                Test Case added

v3.0.2
LUD         :   2nd July 2019
Version     :   3.0.2
LUB         :   Vignesh Chandran
Description :   Standardization of Jar File Names (https://reltio.jira.com/browse/ROCS-42)
                Old Validation Style Replaced (https://reltio.jira.com/browse/ROCS-41)
                Test Case added

v3.0.1
LUD         :   29th Mar 2019
Version     :   3.0.1
LUB         :   Vignesh Chandran
Description :   Clear validation message when properties are missing

v3.0.0
LUD : 22th March 2019
Version 3.0.0
LUB : Shivaputrappa Patil
Description : Fixed bug for decoding utf-8 format

v2.5.7
LUD : 27th Dec 2018
Version 2.5.7
LUB : Vignesh Chandran
Description : Added OPS, Fixed all System.out.println calls to log4j2 log calls


v2.5.6
LUD : 12th Nov 2018
Version 2.5.6
LUB : Sanjay
Description :Fixed issue with deeply nested attributes json generation

v2.5.5
LUD : 12th Nov 2018
Version 2.5.5
LUB : Smita Panda
Description :For adding contributorProvider property in the json generator utility

v2.5.4
LUD : 15th Oct 2018
Version 2.5.4
LUB : Vignesh Chandran
Description : Bug Fix for https://reltio.jira.com/browse/CUST-3030

v2.5.3
LUD : 12th Sept 2018
Version 2.5.3
Description : Bug fix for denormalized nested attributes;
            : Fix for null values for nested attributes.

v2.5.2

Last Update Date: 3rd Sept 2018
Version: 2.5.2
Description: ROCS Standarization changes in Paramertes and core cst changed to 1.4.1


v2.5.0

Last Update Date: 06/06/2018
Version: 2.5.0
Description: Changes in reltio-core-cst to v1.4.0 and revamp of the structure for the maven module. version for json-generator and json-generator-core now sync'd to 2.5.0

Last Update Date: 03/29/2018
Version: 2.4.9
Description: Changes for CUST-2947 with LUD, created date and deleted date support in relations Json Generation
Changes for CUST-2951, ignore crosswalk creation

Last Update Date: 06/30/2017
Version: 1.0.0
Description: Initial version
```

##Contributing 
Please visit our [Contributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/common/src/a8e997d2547bf4df9f69bf3e7f2fcefe28d7e551/CodeOfConduct.md?at=master&fileviewer=file-view-default) to learn more about our contribution guidlines

## Licensing
```
Copyright (c) 2017 Reltio

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

 

    http://www.apache.org/licenses/LICENSE-2.0

 

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and limitations under the License.
```

## Quick Start 
To learn about dependencies, building and executing the tool view our [quick start](https://bitbucket.org/reltio-ondemand/util-jsongenerator-core/src/master/QuickStart.md).


